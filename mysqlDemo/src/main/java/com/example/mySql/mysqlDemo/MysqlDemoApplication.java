package com.example.mySql.mysqlDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;

import org.apache.logging.log4j.LogManager;

@SpringBootApplication

public class MysqlDemoApplication implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory.getLogger(MysqlDemoApplication.class);
	@Autowired
    private BookRepository repository;
	public static void main(String[] args) {
		SpringApplication.run(MysqlDemoApplication.class, args);
	}
  /* @Autowired
    ExpenseRepository repository;
	 @Override
	    public void run(String... args) throws Exception {
	        repository.save(new Expense("Snacks", 15));
	        repository.save(new Expense("Tea", 12));
	        repository.save(new Expense("Rent ", 1200));
	        repository.save(new Expense("Traveling fees", 1350));
	        repository.save(new Expense("other ", 15));
	         
	        Iterable<Expense> iterator = repository.findAll();
	         
	        System.out.println("All expense items: ");
	        iterator.forEach(item -> System.out.println(item));
	         
	        List<Expense> breakfast = repository.findByItem("Snacks");
	        System.out.println("\nHow does my Snacks cost?: ");
	        breakfast.forEach(item -> System.out.println(item));
	         
	        List<Expense> expensiveItems = repository.listItemsWithPriceOver(200);
	        System.out.println("\nExpensive Items: ");
	        expensiveItems.forEach(item -> System.out.println(item));
}*/
	
	@Override
    public void run(String... args) {

        log.info("StartApplication...");

        repository.save(new Book("Java"));
        repository.save(new Book("Node"));
        repository.save(new Book("Python"));

        System.out.println("\nfindAll()");
        repository.findAll().forEach(x -> System.out.println(x));

        System.out.println("\nfindById(1L)");
        repository.findById(1l).ifPresent(x -> System.out.println(x));

        System.out.println("\nfindByName('Node')");
        repository.findByName("Node").forEach(x -> System.out.println(x));

    }
}
